class ChangeRatingColumnTypeToInt < ActiveRecord::Migration[6.1]
  def change
    safety_assured { remove_column :ratings, :rating }
  end
end
