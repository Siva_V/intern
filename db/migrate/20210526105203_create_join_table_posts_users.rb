class CreateJoinTablePostsUsers < ActiveRecord::Migration[6.1]
  def change
    drop_table :posts_users_read_status do |t|
      t.integer "post_id", null: false
      t.integer "user_id", null: false
    end
    create_join_table :posts, :users, table_name: :posts_users_read_status
  end
end
