class AddUserToComment < ActiveRecord::Migration[6.1]
  disable_ddl_transaction!
  def change
    add_reference :comments, :user, index: {algorithm: :concurrently}
  end
end
