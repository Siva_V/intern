class ReFillValueToPost < ActiveRecord::Migration[6.1]
  def up
    Post.reset_column_information
    Post.all.each do |post|
      post.update_attribute(:rating_average, post.ratings.count > 0 ? post.ratings.average(:rating).to_f.round(1) : 0)
    end
  end

  def down

  end
end
