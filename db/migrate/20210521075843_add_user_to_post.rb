class AddUserToPost < ActiveRecord::Migration[6.1]
  disable_ddl_transaction!
  def change
    add_reference :posts, :user, index: {algorithm: :concurrently}
  end
end
