class RemoveRatingAverage < ActiveRecord::Migration[6.1]
  def change
    safety_assured { remove_column :posts, :rating_average }
  end
end
