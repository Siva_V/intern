class RemoveTopicIdFromPost < ActiveRecord::Migration[6.1]
  def change
    remove_column :posts, :topic_id, :integer
  end
end
