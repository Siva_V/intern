namespace :TestRakeTask do
  desc "Creating topic and post"
  task :CreateTopicPost => :environment do
    topic = Topic.new(name: 'Rake', price: '500')
    topic.save
    post = topic.posts.new(name: 'RakePost', content: 'Creating post', topic_id: topic.id, user_id: User.last.id, comments_count: 0, rating_average: 0.0)
    post.save
  end
end