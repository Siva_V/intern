require 'sidekiq/web'
Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  mount LetterOpenerWeb::Engine, at: "/letter_opener"
  devise_for :users, controllers: { registrations: 'users/registrations' }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :topics do
    resources :posts do
      member do
        post :updatestatus
        delete :delete_attached_image
      end
      resources :comments do
        member do
          post :user_comment_rating
        end
      end
      resources :ratings
    end
  end

  resources :posts

  root 'topics#index'
end