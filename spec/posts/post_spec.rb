require 'rails_helper'

RSpec.describe "/posts", type: :request do
  let(:user){
    @user = create(:user)
    sign_in @user
  }
  let(:user1){
    @user1 = create(:user)
    sign_in @user1
  }

  let(:new_image) {Rack::Test::UploadedFile.new('spec/image_file/women-of-Marvel.jpg','image/jpg')}

  let(:create_topic) {
    @topic = Topic.create! vvalid_attributes
  }
  let(:vvalid_attributes){
    { name: 'name', price: '1' }
  }

  let(:valid_attributes) {
    create_topic
    user
    { name: 'name', content: 'content', topic_id: @topic.id, user_id: @user.id, image: new_image, tag_ids: [], tags_attributes: [tag: Faker::Alphanumeric.alpha(number: 10)] }
  }

  let(:invalid_attributes) {
    { name: '', content: 'y', topic_id: @topic.id }
  }
  let(:create_post) {
    @post = Post.create! valid_attributes
  }

  describe "GET /index" do
    context "for normal" do
      it "renders a successful response" do
        create_post
        get topic_posts_url(topic_id: @topic.id)
        expect(response).to be_successful
      end
    end

    context 'for pagination' do
      it '1st' do
        Post.destroy_all
        @topic = Topic.create(name: 'new',price: 100)
        user
        FactoryBot.create_list(:post,19,topic_id:@topic.id, user_id: @user.id)
        get '/posts',params: { page: "1"}
        expect(assigns(:post).map(&:id).count).to eq 10
      end

      it '2nd' do
        Post.destroy_all
        @topic = Topic.create(name: 'new',price: 100)
        user
        FactoryBot.create_list(:post,19,topic_id:@topic.id, user_id: @user.id)
        get '/posts', params: { page: "2" }
        expect(assigns(:post).map(&:id).count).to eq 9
      end
    end

    context "for datepicker" do
      it "initial without dates" do
        Post.destroy_all
        create_topic
        user
        FactoryBot.create_list(:post,8,topic_id: @topic.id,user_id: @user.id)
        get '/posts'
        expect(assigns(:post).map(&:id).count).to eq 8
      end

      it "with from and to dates" do
        Post.destroy_all
        create_topic
        user
        FactoryBot.create_list(:post,5,topic_id: @topic.id,user_id: @user.id)
        temporary = Post.last
        temporary.created_at = Date.yesterday
        temporary.save
        get '/posts', params: { from_date: Date.today, to_date: Date.today }
        expect(assigns(:post).map(&:id).count).to eq 4
      end
    end

  end

  describe "GET /show" do
    it "renders a successful response" do
      create_post
      get topic_posts_url(topic_id: @topic.id)
      expect(response).to be_successful
    end
  end

  describe "POST /updatestatus" do
    it "creates a new read status for current user" do
      create_post
      expect{
      post  updatestatus_topic_post_url(id: @post.id, topic_id: @topic.id)
      }.to change(@post.users,:count).by(1)
    end
  end

  describe "GET /edit" do
    it "renders a successful response" do
      create_post
      get edit_topic_post_url(topic_id:@topic.id, id:@post.id)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Post" do
        create_post
        expect {
          post topic_posts_url(topic_id: @topic.id, id:@post.id), xhr: true, params: { post: valid_attributes }
        }.to change(Post, :count).by(1)

        expect(@post.image.attached?).to be_truthy
      end

      it 'creates a new tag' do
        create_topic
        expect do
          post topic_posts_url(topic_id: @topic.id), xhr: true, params: {post: valid_attributes}
        end.to change(Tag, :count).by(1)
      end

      it "redirects to the created post" do
        create_post
        post topic_posts_url(topic_id: @topic.id, id:@post.id), xhr: true, params: { post: valid_attributes }
        expect(response).to redirect_to(topic_posts_url)
      end
    end

    context "with invalid parameters" do
      it "does not create a new Post" do
        create_post
        expect {
          post topic_posts_url(topic_id:@topic.id), xhr: true, params: { post: invalid_attributes }
        }.to change(Post, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        create_post
        post topic_posts_url(topic_id:@topic.id, id:@post.id), xhr: true, params: { post: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do
      let(:new_attributes) {
        { name:'name', content:"content", topic_id:@topic.id}
      }

      it "when authorized user trying to update" do
        create_post
        patch topic_post_url(topic_id:@topic.id, id:@post.id), params: { post: new_attributes }
        expect(response).to redirect_to(topic_posts_url)
      end

      it "when unauthorized user trying to update" do
        create_post
        sign_out @user
        user1
        expect {
        patch topic_post_url(topic_id: @topic.id, id: @post.id), params: { post: new_attributes }
        }.to raise_error(CanCan::AccessDenied)
      end
    end

    context "with invalid parameters" do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        create_post
        patch topic_post_url(topic_id:@topic.id, id:@post.id), params: { post: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe "DELETE /destroy" do
    context "when authorized user tries to destroy" do
      it 'destroys the selected post' do
        create_post
        expect {
          delete topic_post_url(id: @post.id,:topic_id=>@topic.id)
        }.to change(Post,:count).by(-1)
      end

      it "redirects to the posts list" do
        create_post
        delete topic_post_url(topic_id: @topic.id,id: @post.id)
        expect(response).to redirect_to(topic_posts_url)
      end
    end

    context "when unauthorized user tries to destroy" do
      it "raises AccessDenied error" do
        create_post
        sign_out @user
        user1
        expect {
        delete topic_post_url(topic_id: @topic.id, id: @post.id)
        }.to raise_error(CanCan::AccessDenied)
      end
    end
  end
end