require 'rails_helper'

RSpec.describe "/topics", type: :request do

  before(:each) do
    user = create(:user)
    sign_in user
  end

  # Topic. As you add validations to Topic, be sure to hey
  # adjust the attributes here as well.
  let(:valid_attributes) { {name:'Comic',price:100}
  }

  let(:new_attributes) { { name: "kids",price:300}
  }

  let(:invalid_attributes) { {name:'',price:0}
  }

  describe "GET /index" do
    it "renders a successful response" do
      #Topic.create! valid_attributes
      get topics_url
      expect(response).to be_successful
    end
  end

  describe "GET /show" do

    it "renders a successful response" do
      topic = Topic.create! valid_attributes
      get topic_url(topic)
      expect(response).to be_successful
    end
  end

  describe "GET /new" do
    it "renders a successful response" do
      get new_topic_url
      expect(response).to be_successful
    end
  end

  describe "GET /edit" do
    it "render a successful response" do
      topic = Topic.create! valid_attributes
      get edit_topic_url(topic)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Topic" do
        expect {
          post topics_url, params: { topic: valid_attributes }
        }.to change(Topic, :count).by(1)
      end

      it "redirects to the created topic" do
        post topics_url, params: { topic: valid_attributes }
        expect(response).to redirect_to(topic_url(Topic.last))
      end
    end

    context "with invalid parameters" do
      it "does not create a new Topic" do
        expect {
          post topics_url, params: { topic: invalid_attributes }
        }.to change(Topic, :count).by(0)
      end

      it "renders a successful response (i.e. to display the 'new' template)" do
        post topics_url, params: { topic: invalid_attributes }
        #expect(response).to be_successful
        is_expected.to render_template(:new)
      end
    end
  end

  describe "PATCH /update" do
    context "with valid parameters" do

      it "updates the requested topic" do
        topic = Topic.create! valid_attributes
        patch topic_url(topic), params: { topic: new_attributes }
        topic.reload
        #skip("Completed")
      end

      it "redirects to the topic" do
        topic = Topic.create! valid_attributes
        patch topic_url(topic), params: { topic: new_attributes }
        topic.reload
        expect(response).to redirect_to(topic_path(topic))
      end
    end

    context "with invalid parameters" do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        topic = Topic.create! valid_attributes
        patch topic_url(topic), params: { topic: invalid_attributes }
        #expect(response).to be_successful
        #expect(assigns[:topic].errors[:name]).to eql(['is too short (minimum is 1 character)'])
        expect(response).to render_template(:edit)
      end
    end
  end

  describe "DELETE /destroy" do
    it "destroys the requested topic" do
      topic = Topic.create! valid_attributes
      expect {
        delete topic_url(topic)
      }.to change(Topic, :count).by(-1)
    end

    it "redirects to the topics list" do
      topic = Topic.create! valid_attributes
      delete topic_url(topic)
      expect(response).to redirect_to(topics_url)
    end
  end
end
