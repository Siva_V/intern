FactoryBot.define do
  factory :user_comment_rating do
    rating { "MyText" }
    comment { nil }
    user { nil }
  end


  factory :user do
    email { Faker::Internet.email }
    password { 'password'}
  end

  factory :rating do
    rating { "MyText" }
    post { nil }
  end

  factory :post do
    name { "name" }
    content { "content" }
  end
end