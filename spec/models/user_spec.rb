require 'rails_helper'

RSpec.describe User, type: :request do
  let(:users){
    @user = create(:user)
    sign_in @user
  }

  describe "Sign in" do
    it 'checks index' do
      users
      get topics_url
      expect(response).to be_successful
    end
  end

  describe "Sign out" do
    it 'checks logout' do
      get topics_url
      expect(response).not_to be_successful
    end
  end

  describe "Edit pprofile" do
    it "checks edit" do
      users
      trial = @user.email
      email_id = Faker::Internet.email
      patch user_registration_path, params: { user: { email:  email_id, current_password: 'password' } }
      expect(trial).not_to be_eql(User.last.email)
    end
  end

  describe "Sign up" do
    it "Sign up new user" do
      expect {
        post user_registration_path, params: { user: { email: Faker::Internet.email, password: 'password' } }
      }.to change(User,:count).by(1)
    end

    it "checking whether the email is sent" do
      expect do
        perform_enqueued_jobs do
          post user_registration_path, params: { user: { email: Faker::Internet.email, password: 'password' } }
        end
      end.to change { ActionMailer::Base.deliveries.size }.by(1)
    end
  end

end
