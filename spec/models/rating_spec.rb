require 'rails_helper'

RSpec.describe '/ratings', type: :request do

  before(:each) do
    @user = create(:user)
    sign_in @user
  end

  let(:valid_attributes){
    {name: 'name', price: 1}
  }
  let(:create_topic){
    @topic = Topic.create! valid_attributes
  }
  let(:valided_attributes){
    create_topic
    {name: 'name',content: 'content',topic_id: @topic.id, user_id: @user.id}
  }
  let(:create_post){
    @post = Post.create! valided_attributes
  }
  let(:valids_attributes){
    create_post
    { rating: '5 stars', post_id: @post.id}
  }
  let(:invalid_attributes) {
    { rating: '' }
  }
  let(:create_rating){
    @rating = Rating.create! valids_attributes
  }

  describe "get /index" do
    it "redirects to index" do
      create_rating
      get topic_post_ratings_url(topic_id: @topic.id, post_id: @post.id)
      expect(response).to be_successful
    end
  end

  describe "get /new" do
    it "renders a new view" do
      create_rating
      get new_topic_post_rating_url(topic_id: @topic.id, post_id: @post.id)
      expect(response).to be_successful
    end
  end

  describe "post /create" do
    context "create new rating" do
      it "new rating will be creating" do
        create_rating
        expect{
          post topic_post_ratings_url(topic_id: @topic.id,post_id: @post.id), params: { rating: valids_attributes }
        }.to change(Rating,:count).by(1)
      end
      it "update in rating average" do
        create_post
        post topic_post_ratings_url(topic_id: @topic.id,post_id: @post.id), params: { rating: valids_attributes }
        @post.reload
        expect(@post.rating_average).to be_eql(@post.ratings.average(:rating).to_f.round(1))
      end
    end
  end

end
