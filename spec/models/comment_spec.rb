require 'rails_helper'

RSpec.describe '/comments', type: :request do

  before(:each) do
    @user = create(:user)
    sign_in @user
  end

  let(:user1){
    @user1 = create(:user)
    sign_in @user1
  }
  let(:valid_attributes){
    {name: 'name', price: 1}
  }
  let(:create_topic){
    @topic = Topic.create! valid_attributes
  }
  let(:valided_attributes){
    create_topic
    {name: 'name',content: 'content',topic_id: @topic.id, user_id: @user.id, comments_count: 0, rating_average: 0.0}
  }
  let(:create_post){
    @post = Post.create! valided_attributes
  }
  let(:valids_attributes){
    create_post
    { comment: 'comment', post_id: @post.id, user_id: @user.id}
  }
  let(:invalid_attributes) {
      { comment: '' }
  }
  let(:create_comment){
    @comment = Comment.create! valids_attributes
  }

  describe "get /comment_rating_show" do
    it "renders show" do
      create_comment
      get topic_post_comment_url(@topic,@post,@comment)
      expect(response).to be_successful
    end
  end

  describe "post comment_ratings" do
    it "creates new ratings for comment" do
      create_comment
      expect {
      post user_comment_rating_topic_post_comment_url(@topic,@post,@comment), params: { user_comment_rating: {rating: "5 stars"} }
      }.to change(UserCommentRating,:count).by(1)
    end

    it "redirects to posts/show" do
      create_comment
      post user_comment_rating_topic_post_comment_url(@topic,@post,@comment), params: { user_comment_rating: {rating: "5 stars"} }
      expect(response).to redirect_to(topic_post_path(@topic,@post))
    end
  end

  describe "post /create" do
    context "for valid details" do
      it "succesfully created a new comment" do
        create_comment
        expect{
        post topic_post_comments_url(topic_id: @topic.id,post_id: @post.id), params: { comment: valids_attributes }
        }.to change(Comment,:count).by(1)
      end
      it "renders the posts path" do
        create_comment
        post topic_post_comments_url(topic_id: @topic.id,post_id: @post.id), params: {comment: valids_attributes}
        expect(response).to redirect_to(topic_post_url(@topic,@post))
      end
    end

    context "for counter cache" do
      it "increases the comments count in posts column" do
        create_post
        expect {
          post topic_post_comments_url(topic_id: @topic.id,post_id: @post.id), params: { comment: valids_attributes }
          @post.reload
        }.to change { @post.comments_count }.by(1)
      end
    end

    context "for invalid details" do
      it "not creating a new comment" do
        create_comment
        expect {
          post topic_post_comments_url(topic_id: @topic.id,post_id: @post.id),params: {comment: invalid_attributes}
        }.to change(Comment,:count).by(0)
      end

      it "again renders a new view" do
        create_comment
        post topic_post_comments_url(topic_id: @topic.id,post_id: @post.id), params: { comment: invalid_attributes}
        expect(response).to be_successful
      end
    end
  end

  describe 'get /edit' do
    it 'renders the edit view' do
      create_comment
      get edit_topic_post_comment_url(topic_id: @topic.id,post_id: @post.id,id: @comment.id)
      expect(response).to be_successful
    end
  end

  describe 'patch /update' do
    context "for valid details" do
      it 'redirects to post of the updated comment' do
        create_comment
        put topic_post_comment_url(topic_id: @topic.id,post_id: @post.id,id: @comment.id),params: { comment:valid_attributes }
        expect(response).to redirect_to(topic_post_url(@topic,@post))
      end
    end

    context "for invalid details" do
      it 'renders the edit view again' do
        create_comment
        patch topic_post_comment_url(topic_id: @topic.id, post_id: @post.id,id: @comment.id),params: { comment:invalid_attributes }
        expect(response).to be_successful
      end
    end

    context "for unauthorized user" do
      it "raises an AccessDenied error" do
        create_comment
        sign_out @user
        user1
        expect {
        patch topic_post_comment_url(topic_id: @topic.id, post_id: @post.id, id: @comment.id),params: { comment: valid_attributes }
        }.to raise_error(CanCan::AccessDenied)
      end
    end
  end

  describe 'DELETE /destroy' do
    context "when authorized user tries to destroy" do
      it 'destroys the selected comment' do
        create_comment
        expect {
          delete topic_post_comment_url(topic_id: @topic.id,post_id: @post.id,id: @comment.id)
        }.to change(Comment,:count).by(-1)
      end

      it 'redirects' do
        create_comment
        delete topic_post_comment_url(topic_id: @topic.id,post_id: @post.id,id: @comment.id)
        expect(response).to redirect_to(topic_post_url(@topic,@post))
      end
    end

    context "when unauthorized user tries to destroy" do
      it "raises an AccessDenied error" do
        create_comment
        sign_out @user
        user1
        expect {
        delete topic_post_comment_url(topic_id: @topic.id,post_id: @post.id,id: @comment.id)
        }.to raise_error(CanCan::AccessDenied)
      end
    end
  end
end
