class EmailMailer < ApplicationMailer
  def send_email(user)
    @user = user
    mail(from: 'sivavijayan0610@gmail.com', to: user, subject: 'Sending Email using Letter Opener')
  end
end
