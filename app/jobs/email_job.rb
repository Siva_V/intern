class EmailJob < ApplicationJob
  queue_as :default

  def perform(user_email)
    EmailMailer.send_email(user_email).deliver_now
  end
end
