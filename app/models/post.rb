class Post < ApplicationRecord

  ############### Scope ###############
  scope :posts_datepicker, -> (from_date,to_date) { where("DATE(created_at)>=?", from_date).where("DATE(created_at)<=?", to_date)}

  ############### Association #####################
  belongs_to :topic, inverse_of: :posts
  has_many :comments, dependent: :destroy
  has_and_belongs_to_many :tags
  has_many :ratings, dependent: :destroy
  has_one_attached :image, dependent: :destroy
  belongs_to :user
  has_and_belongs_to_many :users, join_table: 'posts_users_read_status', class_name: "User"

  ############### Validation   ###################
  validates :name, :length => {:minimum => 1, :maximum => 20}
  validates :content, :length => {:minimum =>1, :maximum => 20}

  ############## Accept nested attribute to alter model to model ####################
  accepts_nested_attributes_for :tags, reject_if: Proc.new { |attributes| Tag.where(tag: attributes['tag']).first.present? || attributes['tag'].blank? }
end
