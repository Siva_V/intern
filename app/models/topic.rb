class Topic < ApplicationRecord

  #############  Association ###################
  has_many :posts, dependent: :destroy

  #############  Validation  ###################
  validates :name,length: {minimum: 2}
  validates :price,numericality: { other_than: 0 }

end
