class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  #################### Association ######################
  has_one :post
  has_and_belongs_to_many :posts, join_table: 'posts_users_read_status', class_name: 'Post'
  has_one :comment
  has_many :user_comment_ratings
  has_many :comments, :through => :user_comment_ratings

  after_create :sending_email
  def sending_email
    # EmailJob.perform_later(email)
    EmailMailer.send_email(email).deliver_now
  end
end
