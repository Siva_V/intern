class Rating < ApplicationRecord

  ############# Association ############
  belongs_to :post
  after_create :ratings_average
  def ratings_average
    self.post.update_attribute(:rating_average, self.post.ratings.average(:rating).to_f.round(1))
  end
end
