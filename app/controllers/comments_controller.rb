class CommentsController < ApplicationController

  load_and_authorize_resource
  before_action :get_topic
  before_action :get_post
  before_action :get_comment, only: [:show, :edit, :update, :destroy]

  def show
  end

  def user_comment_rating
    @ratings = UserCommentRating.new(params.require(:user_comment_rating).permit(:rating).merge(user_id: current_user.id, comment_id: params[:id]))
    if @ratings.save
      redirect_to topic_post_path(@topic,@post)
    end
  end

  def create
    @comment = @post.comments.build(comment_params.merge(user_id: current_user.id))
    if @comment.save
      redirect_to topic_post_path(@topic,@post)
    else
      render template: 'posts/show'
    end
  end

  def edit
  end

  def update
    if @comment.update(comment_params)
      redirect_to topic_post_path(@topic,@post)
    else
      render :edit
    end
  end

  def destroy
    @comment.destroy
    redirect_to topic_post_path(@topic,@post)
  end

  private
    def get_topic
      @topic = Topic.find(params[:topic_id])
    end

    def get_post
      @post = @topic.posts.find(params[:post_id])
    end

    def get_comment
      @comment = @post.comments.find(params[:id])
    end

    def comment_params
      params.require(:comment).permit(:comment,:post_id)
    end
end