class PostsController < ApplicationController

  include Pagy::Backend
  load_and_authorize_resource
  before_action :get_topic, except: [:index]
  before_action :get_post, only: [ :show, :edit, :update, :destroy, :delete_attached_image ]
  def index
    if params[:topic_id].nil?
      if params[:from_date].present?
        @pagy, @post = pagy(Post.all.posts_datepicker(params[:from_date],params[:to_date]), page: params[:page], items: 10)
      else
        @pagy, @post = pagy(Post.all.posts_datepicker(Date.today-1,Date.today), page: params[:page], items: 10)
      end
    else
      @topic = Topic.find(params[:topic_id])
      @post = @topic.posts
      @new_post = @topic.posts.build
    end
  end

  def show
    @rating = @post.ratings.new
    @comment = @post.comments.new
  end

  def updatestatus
    @post.users << current_user
  end

  def delete_attached_image
    @image = @post.image
    @image.purge
    redirect_to edit_topic_post_path(@topic,@post)
  end

  def new
    respond_to do |format|
      format.js
    end
  end

  def create
    @post = @topic.posts.build(post_params.merge(user_id: current_user.id, comments_count: 0, rating_average: 0.0))
    respond_to do |format|
      if @post.save
        format.js
      else
        format.js
      end
    end
  end

  def edit
  end

  def update
    if @post.update(post_params)
      redirect_to topic_posts_path
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    redirect_to topic_posts_path
  end

  private
    def get_topic
      @topic = Topic.find(params[:topic_id])
    end

    def get_post
      @post = @topic.posts.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:name, :content, :image, :topic_id,tag_ids: [],tags_attributes: [:id,:tag] )
    end
end