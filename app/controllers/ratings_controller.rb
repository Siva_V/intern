class RatingsController < ApplicationController

  before_action :get_topic
  before_action :get_post

  def index
    @rating = @post.ratings
  end

  def new
    @rating = @post.ratings.build
  end

  def create
    @create = @post.ratings.new(rating_params)
    if @create.save
      redirect_to topic_post_path(@topic,@post)
    end
  end

  private
    def get_topic
      @topic = Topic.find(params[:topic_id])
    end

    def get_post
      @post = @topic.posts.find(params[:post_id])
    end

    def rating_params
      params.require(:rating).permit(:rating)
    end

end